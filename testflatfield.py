import matplotlib.pylab as plt
import astropy.io.fits as pf

fn1 = "output/s1/s1.fits.gz"
fn2 = "output/s2/s2.fits.gz"

d1 = pf.getdata(fn1)
d2 = pf.getdata(fn2)

print (d1-d2).min()
print (d1-d2).max()

plt.imshow(d1-d2, origin="lower", cmap="hot")
plt.colorbar()
plt.show()