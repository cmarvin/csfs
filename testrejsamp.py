import numpy as np
import astropy.io.fits as pyfits
import matplotlib.pylab as plt
from scipy import interpolate

wavesfn = "MODELS/PHOENIX/WAVE_PHOENIX-ACES-AGSS-COND-2011.fits"
fluxfn = "MODELS/PHOENIX/lte03000-5.00-0.0.PHOENIX-ACES-AGSS-COND-2011-HiRes.fits"
       
def rejsamp(xmin, xmax, m, f):
    """Rejection sampling function."""
    while 1:
        xc = np.random.uniform(low=xmin, high=xmax)
        u = np.random.uniform()
        alpha = f(xc) / m
        if alpha >= u:
            return xc   

def main():
    n = 50000
    waves = pyfits.getdata(wavesfn)
    flux = pyfits.getdata(fluxfn)
    f = interpolate.InterpolatedUnivariateSpline(waves, flux)
    m = flux.max()
    wmin = waves.min()
    wmax = waves.max()
    x = np.empty(n)
    for i in xrange(n):
        print "%.2f %%"% (float(i+1)/n * 100.0)
        x[i] = rejsamp(wmin, wmax, m, f)
    y = f(x)

    fig = plt.figure()
    ax1 = fig.add_subplot(121)
    ax2 = fig.add_subplot(122)
    #ax1.scatter(x, y, alpha=0.7)
    ax2.hist(x, bins=1000)
    plt.show()
    
if __name__ == "__main__":
    main()