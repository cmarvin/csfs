#include <stdio.h>
#include <math.h>
#include "armparams.h"

inline double cradians(double angle) {
    return angle * M_PI / 180.0;
}

void compute_c(int ARM,
               int BLAZE_FLAG,
               int LOC_FLAG,
               unsigned long nw,          /* size of w (and also x and y) */
               unsigned long nslit,         /* number of slit elements */
               unsigned short m,
               double xd_0,
               double yd_0,
               double *n_sell,
               double *x,   /* slit location */
               double *y,   /* slit location */
               double *lamb,      /* wavelengths */
               double *w,         /* intensities */
               double *ccd,
               unsigned long *counts,
               unsigned long *m_list,
               double *returnx,
               double *returny) {

    /* assign constants according to spectral arm */
    const double F_COL = (ARM == 0) ? NIR_F_COL : VIS_F_COL;
    const double ALPHA_E = (ARM == 0) ? cradians(NIR_ALPHA_E_DEG) : cradians(VIS_ALPHA_E_DEG);
    const double GAMMA_E = (ARM == 0) ? cradians(NIR_GAMMA_E_DEG) : cradians(VIS_GAMMA_E_DEG);
    const double SIGMA_E = (ARM == 0) ? NIR_SIGMA_E : VIS_SIGMA_E;
    const double ALPHA_G = (ARM == 0) ? cradians(NIR_ALPHA_G_DEG) : cradians(VIS_ALPHA_G_DEG);
    const double SIGMA_G = (ARM == 0) ? NIR_SIGMA_G : VIS_SIGMA_G;
    const double F_CAM = (ARM == 0) ? NIR_F_CAM : VIS_F_CAM;
    const double DPIX = (ARM == 0) ? NIR_DPIX : VIS_DPIX;
    const int NXPIX = (ARM == 0) ? NIR_NXPIX : VIS_NXPIX;
    const int NYPIX = (ARM == 0) ? NIR_NYPIX : VIS_NXPIX;

    /* pre-calculate constants */
    const double ORDER = (double)m;
    const double F_COL_2 = F_COL * F_COL;

    const double MU_E0 = ALPHA_E - M_PI;
    const double NU_E0 = GAMMA_E;

    const double MU_E1 = ALPHA_E + M_PI;
    const double NU_E1 = -GAMMA_E;

    const double NU_G0 = cradians(-8.4);
    const double NU_G1 = ALPHA_G;
    const double NU_G2 = cradians(-11.8);//ALPHA_G;//

    const double COS_MU_E0 = cos(MU_E0);
    const double SIN_MU_E0 = sin(MU_E0);

    const double COS_MU_E1 = cos(MU_E1);
    const double SIN_MU_E1 = sin(MU_E1);

    const double COS_NU_E0 = cos(NU_E0);
    const double SIN_NU_E0 = sin(NU_E0);

    const double COS_NU_E1 = cos(NU_E1);
    const double SIN_NU_E1 = sin(NU_E1);

    const double COS_NU_G0 = cos(NU_G0);
    const double SIN_NU_G0 = sin(NU_G0);

    const double COS_NU_G1 = cos(NU_G1);
    const double SIN_NU_G1 = sin(NU_G1);

    const double COS_NU_G2 = cos(NU_G2);
    const double SIN_NU_G2 = sin(NU_G2);

    /* initialize temporary variables */
    double veclen;
    double li;
    double wi;
    double n_g;

    double xi, xx, x1, x2, x21, x3, x4, x5, xd;
    double yi, yy, y1, y2, y21, y3, y4, y5, yd;
    double zz, z1, z2, z3, z21, z4, z5;
    double x22, x23, x24, x25, x26;
    double y22, y23, y24, y25, y26;
    double z22, z23, z24, z25, z26;
    double inc, incp, theta;

    double beta;
    double blaze_eff;

    unsigned long xbin; /* x coordinate [pix] */
    unsigned long ybin; /* y coordinate [pix] */

    /* index variables */
    unsigned long nind = nw * nslit; /* 1D index */
    unsigned long ind;
    unsigned long i;   /* wavelength iterator */
    unsigned long j;   /* slit iterator */

    for (ind=0; ind<nind; ++ind)
    {
        i = ind / nslit; /* slit iterator */
        j = ind % nslit; /* wave iterator */

        xi = x[j];       /* x coord */
        yi = y[j];       /* y coord */

        li = lamb[i];    /* wavelength */
        wi = w[i];       /* intensities */
        n_g = n_sell[i]; /* refractive indices */

        /* LAUNCH RAYS (create normalized vectors) */
        veclen = sqrt( xi*xi + yi*yi + F_COL_2 );
        xx = xi / veclen;
        yy = yi / veclen;
        zz = F_COL / veclen;

        /* BLAZE FUNCTION */
        //xb = xx;

        /* :::::::::::::::::::: ECHELLE :::::::::::::::::::::::::::::::::::::*/
        /* INTO ECHELLE RF */
        x1 = (ORDER * li / SIGMA_E) - (COS_MU_E0 * xx) + (SIN_MU_E0*SIN_NU_E0 * yy) + (SIN_MU_E0*COS_NU_E0 * zz);
        y1 = -(COS_NU_E0 * yy) + (SIN_NU_E0 * zz);
        z1 = -(SIN_MU_E0 * xx) - (COS_MU_E0*SIN_NU_E0 * yy) - (COS_MU_E0*COS_NU_E0 * zz);
        /* NORMALIZATION AFTER ECHELLE RELATION */
        z1 = (z1 / fabs(z1)) * sqrt(1.0 - y1*y1 - x1*x1);

        /* OUT OF ECHELLE RF */
        x2 = (COS_MU_E1 * x1) - (SIN_MU_E1 * z1);
        y2 = -(SIN_MU_E1*SIN_NU_E1) * x1 + (COS_NU_E1 * y1 - COS_MU_E1*SIN_NU_E1 * z1);
        z2 = (SIN_MU_E1*COS_NU_E1) * x1 + (SIN_NU_E1 * y1 + COS_MU_E1*COS_NU_E1 * z1);
        /* ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/

        
        /* BLAZE FUNCTION */
        beta = M_PI * cos(ALPHA_E) * SIGMA_E * (xx + x2) / li;
        blaze_eff = (sin(beta)*sin(beta)) / (beta*beta);


        /* :::::::::::::::::::: GRISM :::::::::::::::::::::::::::::::::::::::*/
        /* ROTATION INTO PLANE RF */
        x21 = x2;
        y21 = y2 * COS_NU_G0 - z2 * SIN_NU_G0;
        z21 = y2 * SIN_NU_G0 + z2 * COS_NU_G0;
        /* PLANE REFRACTION RELATION */
        theta = atan(x21 / sqrt(y21*y21 + z21*z21 + x21*x21) );
        inc = atan(y21 / z21);
        incp = asin(sin(inc) / n_g);
        x22 = sin(theta) * cos(incp);
        y22 = sin(incp);
        z22 = cos(theta) * cos(incp);
        /* ROTATION OUT OF PLANE RF */
        x23 = x22;
        y23 = y22 * COS_NU_G0 + z22 * SIN_NU_G0;
        z23 = y22 * SIN_NU_G0 - z22 * COS_NU_G0;

        /* ROTATION INTO GRATING RF */
        x24 = x23;
        y24 = y23 * COS_NU_G1 - z23 * SIN_NU_G1;
        z24 = y23 * SIN_NU_G1 + z23 * COS_NU_G1;
        /* GRATING RELATION */
        x25 = x24/ n_g;// ;
        y25 = li / (n_g*SIGMA_G) - y24;//
        z25 = z24 / fabs(z24) * sqrt(1.0 - x25*x25 - y25*y25);
        /* ROTATION OUT OF GRATING RF */
        x26 = x25;
        y26 = (COS_NU_G1 * y25) + (SIN_NU_G1 * z25);
        z26 = (SIN_NU_G1 * y25) - (COS_NU_G1 * z25);

        //printf("x21 = %.2f\ny21 = %.2f\nz21 = %.2f\n", x21, y21, z21);
        //printf("x22 = %.2f\ny22 = %.2f\nz22 = %.2f\n\n", x22, y22, z22);
        /* ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/

        
        /* PROJECTION ONTO DETECTOR */
        //xd = (xd_0/F_CAM  + x25/z25) * (F_CAM / DPIX);
        //yd = -yd_0/DPIX + y25 * (F_CAM/DPIX) / z25;
        xd = -(xd_0 + F_CAM * x26 / y26) / DPIX;
        yd = (yd_0 + F_CAM * y26 / sqrt(x26*x26 + z26*z26)) / DPIX;
        


        /* RETURN EXACT LOCATIONS */
        if (LOC_FLAG == 1) {
            returnx[ind] = xd * DPIX; // returns the location in [mm]
            returny[ind] = yd * DPIX; // returns the location in [mm]
        }

        /* BIN PIXELS */
        else {
            xbin = (int)floor(xd + (double)NXPIX/2.0);
            if (xbin >= 0 && xbin < NXPIX) {
                ybin = (int)floor(yd + (double)NYPIX/2.0);
                if (ybin >= 0 && ybin < NYPIX) {
                    if (BLAZE_FLAG == 1) {
                        ccd[xbin+NXPIX*ybin] += wi * blaze_eff;
                    }
                    else {
                        ccd[xbin+NXPIX*ybin] += wi;
                        counts[xbin+NXPIX*ybin] += 1;
                        m_list[xbin+NXPIX*ybin] = m;
                    }
                }
            }
        }
    }
}