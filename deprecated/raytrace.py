def ray_trace(vp, m, ALPHA_E, GAMMA_E, SIGMA_E, ALPHA_G, N_G, SIGMA_G):
	"""
	This function calculates the steps through the spectrograph.
	
	One can choose to remove the Echelle or Grism element, thus seeing
	the effect of only the other one.
	
	It creates the x- and y- coordinates of the CCD, both in mm and pixels.
	
	Input:	v_init [1D vector of (lambda, x, y, z)]
			m: spectral order
	Output: vp [1D vector of (lambda, x, y, z)]
	
	Arguments: 'ECH' | 'GRISM'
	
	Version 2.0
	changed order of rotation to vp = -RZ -RX -RY M RY RX RZ v
	
	"""
	import numpy as np
	
	def ME(m, sig):
		"""
		Echelle reflection grating transfer matrix
		m:     diffraction order
		sig:   grating constant
		"""
		from numpy import array
		
		M = array([	[1.0,	0.0, 0.0, 0.0],
					[m/sig, -1.0, 0.0, 0.0],
					[0.0,	0.0, -1.0, 0.0],
					[0.0,	0.0, 0.0, -1.0]
					])
		return M
		
	def MG(n, sig):
		"""
		Grism transmission grating transfer matrix
		n:     refractive index
		sig:   grating constant
		This is already rotated by -pi/2
		"""
		from numpy import array
	
		M = array([	[1.0,	   0.0, 0.0, 0.0],
					[0.0, 	   n,	0.0, 0.0],
					[-1.0/sig, 0.0,	n,   0.0],
					[0.0,	   0.0, 0.0, 1.0]
					])
		return M
		
	def MP(n):
		"""
		Plane transfer matrix
		n:     refractive index
		sig:   grating constant
		"""
		from numpy import array
	
		M = array([	[1.0, 0.0,   0.0,   0.0],
					[0.0, 1.0/n, 0.0,   0.0],
					[0.0, 0.0,   1.0/n, 0.0],
					[0.0, 0.0,   0.0,   1.0]
					])
		return M
	
	def RX(nu):
		"""Rotation about X-axis
		nu: [rad]
		[+nu]: counterclockwise
		[-nu]: clockwise
		"""
		from numpy import array,\
						  cos,\
						  sin
		#nu = radians(nu)
		M = array([ [1.0, 0.0, 0.0,		0.0],
					[0.0, 1.0, 0.0, 	0.0],
					[0.0, 0.0, cos(nu),	-sin(nu)],
					[0.0, 0.0, sin(nu),	cos(nu)]
					])
		return M
	
	def RY(mu):
		"""Rotation about Y-axis
		mu: [rad]
		[+mu]: counterclockwise
		[-mu]: clockwise
		"""
		from numpy import array,\
						  cos,\
						  sin
		#mu = radians(mu)
		M = array([ [1.0, 0.0,     0.0,	0.0],
					[0.0, cos(mu), 0.0, -sin(mu)],
					[0.0, 0.0,     1.0,	0.0],
					[0.0, sin(mu), 0.0,	cos(mu)]
					])
		return M
	
	def RZ(tau):
		"""Rotation about Z-axis
		tau: [rad]
		[+tau]: counterclockwise
		[-tau]: clockwise
		"""
		from numpy import array,\
						  cos,\
						  sin
		#tau = radians(tau)
		M = array([ [1.0, 0.0,       0.0,		0.0],
					[0.0, cos(tau),  sin(tau), 	0.0],
					[0.0, -sin(tau), cos(tau),	0.0],
					[0.0, 0.0,       0.0,		1.0]
					])
		return M
	
	
	x = vp[1] * 1.0		# for blaze efficiency
	
	
	# ROTATION INTO ECHELLE
	# Rotation from slit to echelle by off-plane angle gamma_e
	vp = np.dot(RX(GAMMA_E), vp)
	
	# Rotation from slit to echelle by blaze-180
	vp = np.dot(RY(ALPHA_E - np.pi), vp)

	# Echelle relation
	vp = np.dot(ME(m, SIGMA_E), vp)
	# Renormalize z-component
	vp[3] = np.sqrt(1.0 - vp[1]**2 - vp[2]**2) * np.sign(vp[3])
				
	# Rotation from echelle to slit by blaze+180
	vp = np.dot(RY(ALPHA_E + np.pi), vp)
	
	# Rotation from echelle to slit by off-plane angle -gamma_e
	vp = np.dot(RX(-GAMMA_E), vp)
	
	# for blaze efficiency
	xp = vp[1] * 1.0
	const = np.pi * np.cos(ALPHA_E) * SIGMA_E / vp[0]				
	beta = const * (x + xp)
	blaze_eff = (np.sin(beta) / beta)**2


	# ROTATION INTO CD
	# Grism plane relation
	vp = np.dot(MP(N_G), vp)
	# Renormalize z-component
	vp[3] = np.sqrt(1.0 - vp[1]**2 - vp[2]**2) * np.sign(vp[3])
	
	# Rotation from slit to grating by alpha_g
	vp = np.dot(RX(ALPHA_G), vp)
	
	# Grism grating Relation
	vp = np.dot(MG(N_G, SIGMA_G), vp)
	# Renormalize z-component
	vp[3] = np.sqrt(1.0 - vp[1]**2 - vp[2]**2) * np.sign(vp[3])
	
	# Rotation from grating to slit by -alpha_g
	vp = np.dot(RX(-ALPHA_G), vp)
	
	# SLIT TO DETECTOR - commented out since = 1
	#vp = np.dot(RY(np.pi), vp)	

	return vp, blaze_eff

# 	if 'ECH' in args:
# 		vp = transpose(vp)
# 	
# 		# STEP 1
# 		vp = dot(RY(pi - ALPHA_E), vp)
# 	
# 		# STEP 2
# 		vp = dot(RX(GAMMA_E), vp)
# 		
# 		# STEP 3
# 		vp = dot(ME(m, SIGMA_E), vp)
# 		# Renormalize z-component
# 		vp[3] = sqrt(1.0 - vp[1]**2 - vp[2]**2) * sign(vp[3])
# 					
# 		# STEP 4
# 		vp = dot(RX(-GAMMA_E), vp)
# 			
# 		# STEP 5
# 		vp = dot(RY(ALPHA_E - pi), vp)
# 		
# 		# STEP 9
# 		vp = dot(RY(pi), vp)
# 		
# 		return vp
# 	
# 	elif 'GRISM' in args:
# 		vp = transpose(vp)
# 
# 		# Plane
# 		vp = dot(MP(N_G), vp)
# 		vp[3] = sqrt(1.0 - vp[1]**2 - vp[2]**2) * sign(vp[3])
# 		
# 	
# 		# STEP 6
# 		vp = dot(RX(ALPHA_G), vp)
# 		
# 		# STEP 7
# 		vp = dot(MG(N_G, SIGMA_G), vp)
# 		# Renormalize z-component
# 		vp[3] = sqrt(1.0 - vp[1]**2 - vp[2]**2) * sign(vp[3])
# 		
# 		# STEP 8
# 		vp = dot(RX(-ALPHA_G), vp)
# 		
# 		# STEP 9
# 		vp = dot(RY(pi), vp)
# 		
# 		return vp
# 	
# 	else:
# 		#vp = transpose(vp)
# 		
# 		# STEP 1
# 		vp = dot(RY(pi - ALPHA_E), vp) 
# 	
# 		# STEP 2
# 		vp = dot(RX(-GAMMA_E), vp)
# 		
# 		# STEP 3
# 		vp = dot(ME(m, SIGMA_E), vp)
# 		# Renormalize z-component
# 		vp[3] = sqrt(1.0 - vp[1]**2 - vp[2]**2) * sign(vp[3])
# 		
# 		#vp = dot(RZ(pi), vp)
# 					
# 		# STEP 4
# 		vp = dot(RX(GAMMA_E), vp)	
# 			
# 		# STEP 5
# 		vp = dot(RY(ALPHA_E - pi), vp)
# 	
# 		# Plane
# 		vp = dot(MP(N_G), vp)
# 		# Renormalize z-component
# 		vp[3] = sqrt(1.0 - vp[1]**2 - vp[2]**2) * sign(vp[3])
# 	
# 		# STEP 6
# 		vp = dot(RX(ALPHA_G), vp)
# 		
# 		# STEP 7
# 		vp = dot(MG(N_G, SIGMA_G), vp)
# 		# Renormalize z-component
# 		vp[3] = sqrt(1.0 - vp[1]**2 - vp[2]**2) * sign(vp[3])
# 		
# 		# STEP 8
# 		vp = dot(RX(-ALPHA_G), vp)
# 		
# 		# STEP 9
# 		#vp = dot(RY(pi), vp)	
# 	
# 		return vp