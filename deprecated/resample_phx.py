#from csfs import *
import pyfits
import numpy as np
import matplotlib.pyplot as plt

def import_model_spectra():
    """Imports PHOENIX model spectra"""

    print "Importing PHOENIX model spectra..."
    wavefile = 'MODELS/PHOENIX/WAVE_PHOENIX-ACES-AGSS-COND-2011.fits'
    fluxfile = 'MODELS/PHOENIX/lte03000-5.00-0.0.PHOENIX-ACES-AGSS-COND-2011-HiRes.fits'
    wavelengths = pyfits.getdata(wavefile)
    intensities = pyfits.getdata(fluxfile)

    # import model header
    header = pyfits.getheader(wavefile)
    model_header = header
    cards = header.ascardlist()
    keys = []
    values = []
    comments = []
    for item in cards:
        keys.append(item.key)
        values.append(item.value)
        comments.append(item.comment)

    # convert wavelengths in Ang to mm
    #wavelengths *= 1.0e-7

    # NORMALIZES INTENSITY TO 1
    intensities *= 1.0 / intensities.max()

    return wavelengths, intensities, keys, values, comments

def phoenix_spectra():
    """
    The original phoenix spectra has a different sampling above and below
    10000 Angstrom.
    This converts the spectra an evenly sampled phoenix spectra by creating
    a binned average.

    Notes: Need to think of how to apply
    """
    #VIS = SpectralArm('VIS')
    outwaves = 'phx_wavelengths.npy'
    outints = 'phx_intensities.npy'

    wavelengths, intensities, j, k, l = import_model_spectra()

    # info
    #inds = np.where( (wavelengths >= 9999) & (wavelengths <= 10001) )
    #inds = np.where( (wavelengths >= 16998) & (wavelengths <= 17001) )
    #print wavelengths[inds]
    #print wavelengths[0:2]
    #exit()

    inds = np.where( (wavelengths >= 500) & (wavelengths <= 19000) )    # rough estimate for now
    #inds = np.where( (wavelengths >= VIS.ccd_waves_min.min()*1.e7) & (wavelengths <= VIS.ccd_waves_max.max()*1.e7) )
    wavelengths = wavelengths[inds]
    intensities = intensities[inds]
    waves = np.around(wavelengths, decimals=2)

    lind = np.where(waves <= 10000)
    uind = np.where(waves > 10000)
    lwaves = wavelengths[lind]
    lints = intensities[lind]
    uwaves = wavelengths[uind]
    uints = intensities[uind]

    wave_bins = np.arange(lwaves.min(), lwaves.max()+0.02, 0.02)
    new_lints, edges = np.histogram(lwaves, bins=wave_bins, weights=lints)
    new_lints[0] /= 3
    new_lints[1:] /= 2
    xlwaves = (edges[1:] + edges[:-1]) / 2
    i = xlwaves.shape[0]
    xwaves = np.concatenate((xlwaves, uwaves))
    yints = np.concatenate((new_lints, uints))

    np.save(outwaves, xwaves*1.e-7)
    np.save(outints, yints)

def new_resample():
    outwaves = 'phx_wavelengths_resampled.npy'
    outints = 'phx_intensities_resampled.npy'

    wavelengths, intensities, j, k, l = import_model_spectra()
    print wavelengths
    lind = np.where(wavelengths <= 10000)
    lwaves = wavelengths[lind]
    lints = intensities[lind]
    print np.diff(wavelengths)
    print np.diff(wavelengths).max()
    x = np.where( np.diff(wavelengths) == np.diff(wavelengths).min())
    print wavelengths[x]
    print np.bincount(np.int_(lwaves))

    print ''
    print ''

    new_waves = np.arange(wavelengths.min(), 12000.0, np.diff(wavelengths).max())
    print new_waves
    print new_waves.size

    #hist, bin_edges = np.histogram(wavelengths * 1000, bins=new_waves, weights=intensities)
    #print hist
    plt.hist(wavelengths, bins=new_waves, weights=intensities)
    plt.show()

    hist, bin_edges = np.histogram(wavelengths, bins=new_waves, weights=intensities)
    plt.plot(new_waves, hist)
    plt.show()
    #print np.bincount(wavelengths)
     #new_ins = np.bincount(wavelengths, weights=intensities)

new_resample()
#phoenix_spectra()