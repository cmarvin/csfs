import numpy as np
import ctypes

# load c library
#_lib = np.ctypeslib.load_library('functions', '.')

# define numpy array types
array_1d_double = np.ctypeslib.ndpointer(dtype=np.double,
                                         ndim=1,
                                         flags='C_CONTIGUOUS')
array_1d_int = np.ctypeslib.ndpointer(dtype=np.int,
                                      ndim=1,
                                      flags='C_CONTIGUOUS')
array_2d_double = np.ctypeslib.ndpointer(dtype=np.double,
                                         ndim=2,
                                         flags='C_CONTIGUOUS')
array_2d_int = np.ctypeslib.ndpointer(dtype=np.int,
                                      ndim=2,
                                      flags='C_CONTIGUOUS')

# declare arguments and return types for compute_n_bin_wlt
_lib.compute_c.argtypes = [ctypes.c_int,    # ARM ID
                           ctypes.c_int,    # BLAZE_FLAG
                           ctypes.c_int,    # LOC_FLAG
                           ctypes.c_ulong,  # nw
                           ctypes.c_ulong,  # nslit
                           ctypes.c_uint,   # m
                           ctypes.c_double, # xd_0
                           ctypes.c_double, # yd_0
                           array_1d_double, # n_sell
                           array_1d_double, # slitx
                           array_1d_double, # slity
                           array_1d_double, # lamb
                           array_1d_double, # weights
                           array_2d_double, # ccd
                           array_2d_int,    # counts
                           array_2d_int,    # m_list
                           array_1d_double, # returnx
                           array_1d_double  # returny
                           ]
_lib.compute_c.restype = None


class CFunctions(object):

    def __init__(self, mode=None):
        if mode == "echelle":
            self._lib = np.ctypeslib.load_library('functions_ech', '.')
        elif mode == "prism":
            self._lib = np.ctypeslib.load_library('functions_prism', '.')
        elif mode == "grism":
            self._lib = np.ctypeslib.load_library('functions_grism', '.')
        else:
            self._lib = np.ctypeslib.load_library('functions', '.')

    def compute(self,
                arm_flag,
                blaze_flag,
                location_flag,
                nwaves,
                ns,
                m,
                xd_0,
                yd_0,
                n_sell,
                slitx,
                slity,
                waves,
                weights,
                ccd,
                counts,
                m_list,
                returnx,
                returny
                ):
        """c - wrapper"""
        _lib.compute_c(arm_flag,
                    blaze_flag,
                    location_flag,
                    nwaves,
                    ns,
                    m,
                    xd_0,
                    yd_0,
                    n_sell,
                    slitx,
                    slity,
                    waves,
                    weights,
                    ccd,
                    counts,
                    m_list,
                    returnx,
                    returny
                    )
        return None